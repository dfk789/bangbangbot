### bangbangbot is a discord bot. It uses the discordjs node package.

You will neeed node (probably at least v12) to run this bot. 

This was my first decent sized project and I learned a lot doing it. 

The bots capabilities include:
  * Respond to programmed commands. (See /bot/commands/readme.md)
  * Receive webhook events & Announce when specified twitch/mixer streams go live (See /bot/livestreams/readme.md)
  * Add/Remove and remember New streams to announce with commands
  * Dynamic Command replies
  * Configurable Fun Replies (See /bot/funreplies/readme.md)
  * Testing suite

**You will need to configure the bot with your own discord client id in private_config.json**

**You will also need to supply a callback URL to receive events along with a twitch client id for twitch stream announcements, and/or a mixer client id  & mixer auth secret for mixer announcements**


Run tests with `npm test`

Run tests in watch mode with `npm run test:watch`

This will automatically run tests as you make changes.
