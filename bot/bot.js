const DynamicCommands = require('./dynamic-commands');
// const MixerAnnounce = require('./livestreams/mixer-announce')
const FunReplier = require('./funreplies/index')
const Commands = require('./commands/index')
const SocketServer = require('./livestreams/socket-server')
const MixerHooks = require('./livestreams/mixer-hooks')

const numberGenerator = (maxNumber) => {
    return Math.floor((Math.random() * maxNumber));
}

class Bot {
    constructor({ config, funReplies, client, streamIds,fileFolders: { thot } }) {
        this.config = config;
        this.client = client;
        this.funReplies = funReplies
        this.thot = thot;
        this.copyDave = true;
        this.chatChannels = this.client.channels
        this.streamIds = streamIds
        this.dynamicCommands = new DynamicCommands();
        this.socketServer = new SocketServer(this.config,this.chatChannels,this.streamIds)
        this.funReplier = new FunReplier(this.funReplies)
        this.commands = new Commands(this.thot,this.config,this.streamIds);
        this.mixerHooks = new MixerHooks(this.config)
    }   
}

Bot.prototype.start = function () {
    this.client.login(this.config.botClientId)
    this.client.on('ready', () => {
        console.log(`Logged in as ${this.client.user.tag}!`)
        this.socketServer.createServer()
        this.mixerHooks.checkHookRenewal()
        this.mixerHooks.hookRenewer()
    })
    this.client.on('message', msg => this.handleMessage(msg));
    //makes it not crash and go offline when it errors
    //it keeps getting connectionreset error idk what cause is.
    this.client.on('error', err => this.handleError(err));
};

Bot.prototype.handleMessage = function (msg) {
    //ignore bots
    if(msg.author.bot) return;

    //julian is right
    if (msg.content.toLowerCase().match('amiright') && msg.author.id == this.config.coolGuy) {
        msg.channel.send(this.config.amIRightReplies[numberGenerator(this.config.amIRightReplies.length)])
    }

    this.funReplier.checkMsgForReply(msg)
    // You could get the promise from commands(msg) and return if from `handleMessage(msg)`
    
    const resultPromise = this.commands.commands(msg)
    this.dynamicCommands.learnNewCommandIfRequired(msg.content, errResponse => msg.channel.send(errResponse));
    this.dynamicCommands.withCommandResponse(msg.content, response => msg.channel.send(response));
    return resultPromise
};

Bot.prototype.handleError = function (err) {
    console.log(err.message)
    console.log(err.name)
};


module.exports = Bot;