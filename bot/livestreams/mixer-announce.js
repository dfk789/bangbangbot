const MixerHooks = require('./mixer-hooks')


class MixerAnnounce {
  constructor(){
    this.mixerHooks = new MixerHooks()
  }
}

MixerAnnounce.prototype.announceMessage = async function (id) {
  try{
    let channelInfo = await this.mixerHooks.getChannelInfo(id)
    return {
      author: {
        "name":`${channelInfo.user.username} is playing ${channelInfo.type.name}`,
        "url":`https://mixer.com/${channelInfo.user.username}`,
        "icon_url":`${channelInfo.user.avatarUrl}`},
      "description":`
        ${channelInfo.name}  
        
        [Watch at mixer.com/${channelInfo.user.username}](https://mixer.com/${channelInfo.user.username})`
        + "```📕: "+this.shortDescription(channelInfo.type.description)+"```",
      color:0xff0006,
      thumbnail:{"url": `${channelInfo.user.avatarUrl}`},
      image:{url:`${this.boxArt(channelInfo)}`},
      footer:{
        "text":`Viewers: ${channelInfo.viewersCurrent}`
      },
      timestamp:new Date
    }
  } catch(error) {
    console.log(error)
  } 
};

MixerAnnounce.prototype.boxArt = function (channelInfo) {
  if (channelInfo.type.backgroundUrl) {
    return channelInfo.type.backgroundUrl
  }
  if (channelInfo.type.coverUrl) {
    return channelInfo.type.coverUrl
  }
  if (!channelInfo.type.backgroundUrl && !channelInfo.type.coverUrl) {
    return channelInfo.thumbnail.url
  }
};

MixerAnnounce.prototype.shortDescription = function (description) {
  if(description){
    if(description.length > 100) {
      description = description.slice(0,100) + '...'
      return description
    } else {
      return description
    }
  } else {
    return 'No description found.'
  }
};

module.exports = MixerAnnounce