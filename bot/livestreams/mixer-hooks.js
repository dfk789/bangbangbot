const fetch = require('node-fetch')

class MixerHooks {
  constructor(config) {
    this.config = config
    this.getChannelId = async function (name) {
      let channel = await this.getChannelInfo(name)
      return channel.id
    }
  }
}

MixerHooks.prototype.getChannelInfo = function (id) { //id or name
  console.log(`attemtping to fetch https://mixer.com/api/v1/channels/${id}`)
  return fetch(`https://mixer.com/api/v1/channels/${id}`, {
    method:'GET',
    withCredentials: true,
    headers:{
      "Client-ID": this.mixerClientId
    }
  })
   .then(res => res.json())
   .catch(err => console.error(err))
};

MixerHooks.prototype.createMixerHook = function (channelId) {
  console.log(`attempting to create webhook for ${channelId}...`)
  fetch('https://mixer.com/api/v1/hooks/', {
    method: 'POST',
    withCredentials: true,
    headers: {
      "Client-ID": this.config.mixerClientId,
      "Authorization": this.config.mixerAuthorization
    },
    body:JSON.stringify({
      "kind":'web',
      "events":[
        `channel:${channelId}:broadcast`
      ],
      "url":'http://90.255.232.30:3000/mixer'
    })
  })
   .then(res => res.json())
   .catch(err => console.error(err))
};

MixerHooks.prototype.getClientHooks = function () {
  console.log('attempting to get webhooks subscriptions...')
  return fetch('https://mixer.com/api/v1/hooks', {
    method: 'GET',
    withCredentials: true,
    headers: {
      "Client-ID": this.config.mixerClientId,
      "Authorization": this.config.mixerAuthorization
    }
  })
   .then(resolve => resolve.json())
   .catch(err => console.error(err))
};

//hooks are valid for 90days this will renew them if they have less than 7 days remaining
MixerHooks.prototype.checkHookRenewal = async function () {
  const hooks = await this.getClientHooks(), oneWeek = 604800000
  hooks.forEach(webHook => {
    const expiresAt = Date.parse(webHook.expiresAt)
    if(expiresAt - Date.now() < oneWeek && webHook.isActive) {
      console.log(`renewing ${webHook.id}`)
      this.renewHook(webHook.id)
    } else if(webHook.isActive) {
      console.log(`This hook(${webHook.id}) for ${webHook.events} is valid until ${webHook.expiresAt}`)
    }
  });
};

MixerHooks.prototype.renewHook = function (hookId) {
  fetch(`https://mixer.com/api/v1/hooks/${hookId}/renew`, {
    method: 'POST',
    withCredentials: true,
    headers: {
      "Client-ID": this.config.mixerClientId,
      "Authorization": this.config.mixerAuthorization
    }
  })
  .then(response => response.json(), console.log(response.json()))
  .catch(err => console.error(err))
};

MixerHooks.prototype.deactivateHook  = function (hookId) {
  console.log(`attempting to deactive ${hookId}`)
  fetch(`https://mixer.com/api/v1/hooks/${hookId}/deactivate`, {
    method: 'POST',
    withCredentials:  true,
    headers: {
      "Client-ID": this.config.mixerClientId,
      "Authorization": this.config.mixerAuthorization
    }
  })
   .then(response => response.json(), console.log(response.json()))
   .catch(err => console.error(err))
};

MixerHooks.prototype.checkForExistingHook = async function (channelId) {
  let hooks = await this.getClientHooks(), isDupe = {}
  hooks.forEach(webHook => {
    let match = webHook.events[0].match(/\d+/)
    if(match[0] == channelId){
      console.log('already exists')
      isDupe.dupe = true
      isDupe.isActive = webHook.isActive
      isDupe.hookId = webHook.id
      return isDupe
    } else {
      console.log('doesnt exist')
      isDupe.dupe = false
      isDupe.isActive = webHook.isActive
      isDupe.hookId = webHook.id
      return isDupe
    }
  })
  return isDupe
};

MixerHooks.prototype.getActiveHookId = async function (channelId) {
  let hooks = await this.getClientHooks(), hookId
  hooks.forEach(webHook => {
    let match = webHook.events[0].match(/\d+/)
    if(match[0] == channelId && webHook.isActive == true){
      hookId = webHook.id
      return hookId
    }
  })
  console.log(`returning final hook id as ${hookId}`)
  return hookId
};

//call check hook renewal every 6 days
MixerHooks.prototype.hookRenewer = function () {
  setInterval(() => {
    this.checkHookRenewal()
  }, 518400000); // 6 days
};


module.exports = MixerHooks;

