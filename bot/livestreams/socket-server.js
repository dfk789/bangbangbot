const TwitchAnnounce = require('./twitch-announce')
const MixerAnnounce = require('./mixer-announce')

/*
  THIS RECEIVES EVENTS FROM WEBHOOK-SERVER 
*/

// Node.js socket server script
const net = require('net');
// Create a server object
class SocketServer  {
  constructor(config, chatChannels,streamIds) {
    this.config = config;
    this.chatChannels = chatChannels;
    this.streamIds = streamIds
    this.twitchAnnounce = new TwitchAnnounce(this.config,this.chatChannels,this.streamIds);
    this.mixerAnnounce = new MixerAnnounce()    
  }
    
}

SocketServer.prototype.createServer = function () {
  console.log('creating socket server...')
  this.twitchSubscriptions()
  const server = net.createServer((socket) => {
    socket.on('data', async (data) => {
      this.eventHandler(data.toJSON())
    });
  }).on('error', (err) => {
    console.error(err);
  });
  // Open server on port 9898
  server.listen(9898, () => {
    console.log('opened server on', server.address().port);
  });
}

SocketServer.prototype.eventHandler = function (hookData) {
  console.log('receiving data...')
  if(hookData.eventOrigin == 'twitch') {
    this.twitchHandler(hookData)
  } else if (hookData.eventOrigin == 'mixer') {
    this.mixerHandler(hookData)
  }
}

SocketServer.prototype.twitchSubscriptions = async function () {
  this.twitchAnnounce.startSubscriptions(this.streamIds.twitchIds)
  setInterval(() => {//renew every 24 hours
    this.twitchAnnounce.startSubscriptions(this.streamIds.twitchIds)
  }, 21600000);//6hrs
}


SocketServer.prototype.mixerHandler = async function (hookData) {
  console.log(`event origin is ${hookData.eventOrigin}`)
  let re = /\d+/
  let mixerId = hookData.event.match(re)
  this.chatChannels.get(this.config.streamAnnounceChannel)
   .send({embed:await this.mixerAnnounce.announceMessage(mixerId[0])})

}

SocketServer.prototype.twitchHandler = async function (hookData) {
  console.log(`event origin is ${hookData.eventOrigin}`)
  hookData = hookData.data[0]
  console.log(hookData)
  this.chatChannels.get(this.config.streamAnnounceChannel)
    .send({embed:await this.twitchAnnounce.announceMessage(hookData)})

}


module.exports = SocketServer