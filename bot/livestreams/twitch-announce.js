const fetch = require('node-fetch')
const request = require('request');

function catchError (n) {
  console.log('ERROR ' + n)
}

function checkStatus (res) {
  if (res.ok) { // res.status >= 200 && res.status < 300
    return res
  } else {
    return console.log('no good')
  }
}

class TwitchAnnounce  {
  constructor(config,chatChannels,streamIds) {
    this.config = config;
    // this.isLive = false;
    // this.onlineAt = 0
    this.chatChannels = chatChannels
    this.streamIds = streamIds
    
  }   
}

TwitchAnnounce.prototype.getStreamInfo = async function (name) {
  return fetch(`https://api.twitch.tv/helix/streams?user_login=${name}`, {headers:{'Client-ID':this.config.twitchClientId}})
    .then(checkStatus).catch(catchError)
    .then(res => res.json())
    .then(streamInfo => {
        return streamInfo.data[0]
    }).catch(catchError)
}

TwitchAnnounce.prototype.getUserInfo = async function (name) {
  return fetch(`https://api.twitch.tv/helix/users?login=${name}`, {headers:{'Client-ID':this.config.twitchClientId}})
    .then(checkStatus).catch(catchError)
    .then(res => res.json())
    .then(userInfo => {
      return userInfo.data[0]
    }).catch(catchError)
}

TwitchAnnounce.prototype.getUserInfoById = async function (id) {
  return fetch(`https://api.twitch.tv/helix/users?id=${id}`, {headers:{'Client-ID':this.config.twitchClientId}})
    .then(checkStatus).catch(catchError)
    .then(res => res.json())
    .then(userInfo => {
      return userInfo.data[0]
    }).catch(catchError)
}

TwitchAnnounce.prototype.getGameInfo = async function (gameId) {
  return fetch(`https://api.twitch.tv/helix/games?id=${gameId}`, {headers:{'Client-ID':this.config.twitchClientId}})
    .then(checkStatus).catch(catchError)
    .then(res => res.json())
    .then(gameInfo => {
      return gameInfo.data[0]
    }).catch(catchError)
}

TwitchAnnounce.prototype.getHookSubscriptions = async function () {//no work needs access token
  return fetch(`https://api.twitch.tv/helix/webhooks/subscriptions`, {headers:{'Client-ID':this.config.twitchClientId}})
    .then(checkStatus).catch(catchError)
    .then(res => console.log(res))
    .catch(catchError)
}

TwitchAnnounce.prototype.announceMessage = async function (streamData) {
  try{
    const gameInfo = await this.getGameInfo(streamData.game_id), userInfo = await this.getUserInfoById(streamData.user_id)
    var boxart = gameInfo.box_art_url.replace('-{width}x{height}','-220x290')
    if(boxart.match('./')) boxart = boxart.replace('./','') //Siege boxart has a ./ in url. Needs be changed to show img in discord.
    return {
      author: {
        "name":`${streamData.user_name} is playing ${gameInfo.name}`,
        "url":`http://twitch.tv/${streamData.user_name}`,
        "icon_url":`${userInfo.profile_image_url}`},
      description:`
        ${streamData.title}  
        
        [Watch at twitch.tv/${streamData.user_name}](https://twitch.tv/${streamData.user_name})`,
      color:13797116,
      thumbnail:{"url": `${userInfo.profile_image_url}`},
      image:{url:`${boxart}`},
      footer:{"text":`Viewers: ${streamData.viewer_count}`},
      timestamp:new Date
    }
  } catch(error) {
    console.log(error)
  } 
};

TwitchAnnounce.prototype.twitchHookSubscriber = async function (event,id,subMode) {
  try {
    let options = {
      url: 'https://api.twitch.tv/helix/webhooks/hub',
      method: 'POST',
      headers: {
        'Client-ID': this.config.twitchClientId,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'hub.mode':subMode,
        'hub.topic':`https://api.twitch.tv/helix/${event}?user_id=${id}`,
        'hub.callback':`${this.config.callbackURL}/twitch/${event}/${id}`,
        'hub.lease_seconds':21600//6hrs 
      })
    };
    request.post(options);
  } catch(error) {
    console.log(error)
  }
};

TwitchAnnounce.prototype.startSubscriptions = function (idArray) {
  try{
    console.log('starting subscriptions')
    idArray.forEach(id => {
      console.log(`subscribing to ${id}`)
      this.twitchHookSubscriber('streams',id,'subscribe')
    });
  } catch(error) {
    console.log(error)
  }
};

TwitchAnnounce.prototype.stopSubscriptions = function (idArray) {
  try{
    idArray.forEach(id => {
      console.log(`unsubbbing from ${id}...`)
      this.twitchHookSubscriber('streams',id,'unsubscribe')
    });
  } catch(error) {
    console.log(error)
  }
};

TwitchAnnounce.prototype.stopSubscription = function (id) {
  try{
    console.log(`unsubbbing from ${id}...`)
    this.twitchHookSubscriber('streams',id,'unsubscribe')
  } catch(error) {
    console.log(error)
  }
};

module.exports = TwitchAnnounce