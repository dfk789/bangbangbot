const TwitchAnnounce = require('../livestreams/twitch-announce');
const MixerHooks = require ('../livestreams/mixer-hooks')
const {addTwitchId, delTwitchId} = require('./twitch_stream')
const {addMixerId, delMixerId} = require('./mixer_stream')
const {thot} = require('./thot')
const {help} = require('./help')
const {daveName} = require('./dave')
const {channelName} = require('./channelname')


class Commands  {
  constructor(thotDir,config,streamIds){
    this.thotDir = thotDir;
    this.config = config;
    this.streamIds = streamIds
    this.twitchAnnounce = new TwitchAnnounce(this.config)
    this.mixerHooks = new MixerHooks(this.config)
    this.prefix = this.config.prefix
    this.saveStreams = true
  }
}


Commands.prototype.commands = function (msg) {
  if(!msg.content.startsWith(this.config.prefix[0])) return

  thot(msg, this.prefix, this.thotDir)
  help(msg, this.prefix)
  channelName(msg, this.prefix, this.config.funChannels)
  daveName(msg, this.prefix)

  return Promise.all([
    addTwitchId(msg, this.prefix, this.twitchAnnounce, this.streamIds, this.saveStreams),
    delTwitchId(msg, this.prefix, this.twitchAnnounce, this.streamIds, this.saveStreams),
    addMixerId(msg, this.prefix, this.mixerHooks, this.streamIds, this.saveStreams),
    delMixerId(msg, this.prefix, this.mixerHooks, this.streamIds, this.saveStreams)
  ])
};

module.exports = Commands