
const {streamCommandGuards, getArgs, checkDupeId, saveJson} = require('./helpers')

async function addTwitchId (msg, prefix, twitchAnnounce, streamIds, saveBool) {
  if (streamCommandGuards(msg,'addtwitch', prefix) == false) return
  const args = getArgs(msg, prefix), name = args[1], userInfo = await twitchAnnounce.getUserInfo(name)
  if (!userInfo) return msg.channel.send('Unable to find this user.')   
  if (!checkDupeId(streamIds.twitchIds,userInfo.id)) {
    streamIds.twitchIds.push(userInfo.id)
    if (saveBool) saveJson('./config/stream_ids.json',streamIds)
    twitchAnnounce.startSubscriptions(streamIds.twitchIds)
    msg.channel.send(`I will now announce when ${name}(${userInfo.id}) goes live.`)
  } else {
    msg.channel.send('I already announce this stream.')
  }
};

async function delTwitchId (msg, prefix, twitchAnnounce, streamIds, saveBool) {
  if (streamCommandGuards(msg,'deltwitch', prefix) == false) return

  const args = getArgs(msg, prefix), name = args[1], userInfo = await twitchAnnounce.getUserInfo(name)
  
  if (!userInfo) return msg.channel.send('Unable to find this user.')
      
  for (let index = 0; index < streamIds.twitchIds.length; index++) {
    const id = streamIds.twitchIds[index];
    if (id == userInfo.id) {
      twitchAnnounce.stopSubscription(userInfo.id)
      streamIds.twitchIds.splice(index,1)
      if (saveBool) saveJson('./config/stream_ids.json',streamIds)
      msg.channel.send(`I will no longer announce ${name}(${userInfo.id}) streams.`)
    }
  }
};

module.exports = {delTwitchId, addTwitchId}