const commandsHelp = require('./commands_help.json');
const {getCommand} = require('./helpers')

function help (msg, prefix) {
  if (getCommand(msg, prefix) == 'help' || getCommand(msg, prefix) == 'commands') {
    let commandString = ''
    commandsHelp.forEach(command => {
      commandString += `\rPrefix: "${command.prefix}" Command: "${command.command}" What it do: "${command.description}"\r`
    });
    msg.reply({ embed: { description: commandString } })
  }
};

module.exports = {help}