const {getCommand} = require('./helpers')

function thot (msg, prefix, thotDir) {
  if (getCommand(msg, prefix) == 'thot') {
    msg.channel.send({
      'file': `./media/thots/${thotDir[Math.floor((Math.random() * thotDir.length))]}`
    })
  }
};

module.exports = {thot}