const {streamCommandGuards, getArgs, checkDupeId, saveJson} = require('./helpers')

async function addMixerId (msg, prefix, mixerHooks, streamIds, saveBool) {
  if (streamCommandGuards(msg,'addmixer', prefix) == false) return;

  const args = getArgs(msg, prefix), name = args[1], channelId = await mixerHooks.getChannelId(name)

  if (!channelId) return msg.channel.send('Sorry, I cannot find this user.')
      
  if (!checkDupeId(streamIds.mixerIds,channelId)) {
    streamIds.mixerIds.push(channelId)
    if (saveBool) saveJson('./config/stream_ids.json',streamIds)
    mixerHooks.createMixerHook(channelId)
    msg.channel.send(`I will now announce when ${name}(${channelId}) goes live.`)
  } else {
    msg.channel.send(`I should already announce this stream.`)
  }
};

async function delMixerId (msg, prefix, mixerHooks, streamIds, saveBool) {
  if (streamCommandGuards(msg,'delmixer', prefix) == false) return;

  const args = getArgs(msg, prefix),
        name = args[1],
        channelId = await mixerHooks.getChannelId(name),
        hookId = await mixerHooks.getActiveHookId(channelId)

  if (!channelId) return msg.channel.send('Sorry, I cannot find this user.')
      
  for (let index = 0; index < streamIds.mixerIds.length; index++) {
    let id = streamIds.mixerIds[index]
    if (id == channelId) {
      mixerHooks.deactivateHook(hookId)
      streamIds.mixerIds.splice(index,1)
      if (saveBool) saveJson('./config/stream_ids.json',streamIds)
      msg.channel.send(`I will no longer announce ${name}(${channelId}) streams.`)
    }
  }
};

module.exports = {addMixerId, delMixerId}