const fs = require('fs')

function streamCommandGuards (msg,command, prefix) {
  const commandMsg = getCommand(msg, prefix)
  if (commandMsg !== command) return false
  const args = getArgs(msg, prefix)
  if (!args[1]) {
    msg.channel.send('please enter a username')
    return false
  }    
}

function getCommand (msg, prefix) {
  const args = msg.content.slice(prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  return command
};

function getArgs (msg, prefix) {
  const args = msg.content.slice(prefix.length).trim().split(/ +/g)
  return args
};

function checkDupeId(idArray,id) {
  if(idArray.indexOf(id) == -1){
      console.log(`${id} doesnt exist already`)
      return false
  } else {
      console.log(`${id} already exists`)
      return true
  }
};

function saveJson (filePath,file) {
  fs.writeFile(filePath,JSON.stringify(file, null, 2),(error)=>{
    if (error) return console.log(error);
    //console.log(JSON.stringify(file, null, 2));
    console.log('writing to ' + filePath);
  })
};

module.exports = {streamCommandGuards, getCommand, getArgs, checkDupeId, saveJson}