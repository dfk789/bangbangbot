const {getCommand} = require('./helpers')

function channelName (msg, prefix, funChannels) {
  if (getCommand(msg, prefix) != 'channelname') return
  funChannels.forEach(channel => {
    if (msg.channel.id == channel) return msg.channel.edit({name:msg.content.slice(12)})
  });
};

module.exports = {channelName}