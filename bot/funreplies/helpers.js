setFile = function (funReply) {
  if (funReply.file) return funReply.file
  if (funReply.directory) return randomFile(funReply.directory, funReply.fsDir)
}

setReply = function (funReply) {
  if (!Array.isArray(funReply.replies)) return funReply.replies
  if (Array.isArray(funReply.replies)) return randomReply(funReply.replies)
}

randomReply = function (array) {
  return array[Math.floor(Math.random() * array.length)]
}

randomFile = function (directory, fsDir) {
  return directory+'/'+fsDir[Math.floor(Math.random() * fsDir.length)]
}

sendFunReply = function (msg,reply) {
  //console.log(reply)
  //if(reply.message) console.log(reply.message)
  if (reply.message && reply.file) {
    return msg.channel.send(reply.message, {file:reply.file})
  }
  if (reply.message && !reply.file) {
    return msg.channel.send(reply.message)
  }
  if (!reply.message && reply.file) {
    return msg.channel.send({file:reply.file})
  }
}

module.exports = {setFile, setReply, sendFunReply}