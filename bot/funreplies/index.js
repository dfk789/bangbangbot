const {checkFileReply} = require('./file_reply')
const {checkMsgReply} = require('./msg_reply')
const {checkMsgFileReply} = require('./msg_file_reply')

class FunReplier  {
  constructor(funReplies) {
    this.funReplies = funReplies;
  }
}

FunReplier.prototype.checkMsgForReply = function (msg) {
  this.funReplies.forEach(funReply => {
    checkFileReply(msg, funReply)
    checkMsgReply(msg, funReply)
    checkMsgFileReply(msg, funReply)
  });
}



module.exports = FunReplier