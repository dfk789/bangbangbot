const {setFile, setReply, sendFunReply} = require('./helpers')

msgFileReply = function (msg, command, reply, file) {
  if (msg.content == command) return sendFunReply(msg,{message:reply, file:file})
}
msgFileReplyMatch = function (msg, command, reply, file) {
  if (msg.content.match(command)) return sendFunReply(msg,{message:reply, file:file})
}
msgFileReplyRegex = function (msg, command, reply, file) {
  if (new RegExp(command).test(msg.content)) return sendFunReply(msg,{message:reply, file:file})
}

msgFileGuard = function (funReply) {
  let pass = false
  if (funReply.file || funReply.directory) {
    if (funReply.replies) pass = true
    if (!funReply.replies) pass = false
  }
  return pass
}

checkMsgFileReply = function (msg, funReply) {
  if (!msgFileGuard(funReply)) return
  if (!Array.isArray(funReply.commands)) {
    if (!funReply.match && !funReply.regex) return msgFileReply(msg, funReply.commands, setReply(funReply), setFile(funReply))
    if (funReply.match && !funReply.regex) return msgFileReplyMatch(msg, funReply.commands, setReply(funReply), setFile(funReply))
    if (!funReply.match && funReply.regex) return  msgFileReplyRegex(msg, funReply.commands, setReply(funReply), setFile(funReply))
  }
  if (Array.isArray(funReply.commands)) {
    funReply.commands.forEach(command => {
      if (!funReply.match && !funReply.regex) return msgFileReply(msg, command, setReply(funReply), setFile(funReply))
      if (funReply.match && !funReply.regex) return msgFileReplyMatch(msg, command, setReply(funReply), setFile(funReply))
      if (!funReply.match && funReply.regex) return msgFileReplyRegex(msg, command, setReply(funReply), setFile(funReply))
    });
  }
}

module.exports = {checkMsgFileReply}


