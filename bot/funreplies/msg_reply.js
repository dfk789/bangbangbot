const {setReply, sendFunReply} = require('./helpers')

msgReply = function (msg, command, reply) {
  if (msg.content == command) return sendFunReply(msg,{message:reply})
}

msgReplyMatch = function (msg, command, reply) {
  if (msg.content.match(command)) return sendFunReply(msg,{message:reply})
}

msgReplyRegex = function (msg, command, reply) {
  if (new RegExp(command).test(msg.content)) return sendFunReply(msg,{message:reply})
}

checkMsgReply = function (msg, funReply) {
  if (funReply.file || funReply.directory) return

  if (!Array.isArray(funReply.commands)) {
    if (!funReply.match && !funReply.regex) return msgReply(msg, funReply.commands, setReply(funReply))
    if (funReply.match && !funReply.regex) return msgReplyMatch(msg, funReply.commands, setReply(funReply))
    if (!funReply.match && funReply.regex) return msgReplyRegex(msg, funReply.commands, setReply(funReply))
  }
  if (Array.isArray(funReply.commands)) {
    funReply.commands.forEach(command => {
      if (!funReply.match && !funReply.regex) return msgFileReply(msg, command, setReply(funReply))
      if (funReply.match && !funReply.regex) return msgFileReplyMatch(msg, command, setReply(funReply))
      if (!funReply.match && funReply.regex) return msgFileReplyRegex(msg, command, setReply(funReply))
    });
  }
}


module.exports = {checkMsgReply}
