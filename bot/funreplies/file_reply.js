const {setFile, sendFunReply} = require('./helpers')

fileReply = function (msg, command, file) {
  if (msg.content == command) return sendFunReply(msg,{file:file})
}

fileReplyMatch = function (msg, command, file) {
  if ( msg.content.match(command)) return sendFunReply(msg,{file:file})
}

fileReplyRegex = function (msg, command, file) {
  if (new RegExp(command).test(msg.content)) return sendFunReply(msg,{file:file})
}

checkFileReply = function (msg, funReply) {
  if (funReply.replies) return
  if (!Array.isArray(funReply.commands)) {
    if (!funReply.match && !funReply.regex) return fileReply(msg, funReply.commands, setFile(funReply))
    if (funReply.match && !funReply.regex) return fileReplyMatch(msg, funReply.commands, setFile(funReply))
    if (!funReply.match && funReply.regex) return fileReplyRegex(msg, funReply.commands, setFile(funReply))
  }

  if (Array.isArray(funReply.commands)) {
    funReply.commands.forEach(command => {
      if (!funReply.match && !funReply.regex) return fileReply(msg, command, setFile(funReply))
      if (funReply.match && !funReply.regex) return fileReplyMatch(msg, command, setFile(funReply))
      if (!funReply.match && funReply.regex) return fileReplyRegex(msg, command, setFile(funReply))
    });
  }
}

module.exports = {checkFileReply}