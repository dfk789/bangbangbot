class DynamicCommands {
  constructor() {
    this.commands = {};
  }
};

DynamicCommands.prototype.learnNewCommandIfRequired = function (message, charLimitResponse) {
  const regex = /When I say (.+) (the bot) says (.+)/i;
  const match = message.match(regex);
  if (match) {
    if (message.length > 250) return charLimitResponse('Too many characters. Try less than 250')
    this.commands[match[1]] = match[3] || match[2];
  }
};

DynamicCommands.prototype.withCommandResponse = function (message, responseHandler) {
  let keyArray = Object.keys(this.commands)
  keyArray.forEach(key => {
    if(new RegExp(`\\b${key}\\b`).test(message)==true){ //check message for key/command
      let response = this.commands[new RegExp(`\\b${key}\\b`).exec(message)[0]]
      responseHandler(response)
    }
  });
};

module.exports = DynamicCommands;
