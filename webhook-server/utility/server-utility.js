function ServerUtility () {

}

ServerUtility.prototype.getRequestBody = function (request) {
  return new Promise(resolve => {
    const bodyBytes = [];
    request.on('data', chunk => {
      bodyBytes.push(chunk);
    }).on('end', () => {
      const body = Buffer.concat(bodyBytes).toString();
      let parsedBody = JSON.parse(body)
      resolve(parsedBody)
    })
  })
};

ServerUtility.prototype.socketConnect = function (hookData) {
  if(!hookData || hookData.data == ''){
    console.log(hookData)
    console.log(`webhook data is not truthy, or data is blank. returning...`)
    return
  }
  const net = require('net');
  const client = net.createConnection({ port: 9898 }, () => {
    console.log('CLIENT: I connected to the server.');
  });
  client.write(JSON.stringify(hookData))
  console.log(hookData)
  console.log('CLIENT: Sending Data...')
  client.end();
  client.on('end', () => {
    console.log('CLIENT: I disconnected from the server.');
  });
  client.on('error',(error) =>{
    console.log(error)
  })

};

module.exports = ServerUtility