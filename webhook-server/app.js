/*
    This can probably be made to run in the bot process.
    But I have decided to run it as a seperate process and communicate with a websocket. 
    This should allow this server to be more flexible in future...
*/
const http = require('http');
const TwitchHandler = require('./platforms/twitch-handler')
const MixerHandler = require('./platforms/mixer-handler')


function WebHookServer () {
    this.mixerEventId = '';
    this.twitchHandler = new TwitchHandler();
    this.mixerHandler = new MixerHandler();
}
const webHookServer = new WebHookServer();

WebHookServer.prototype.createServer = function () {
    console.log('creating webhook server')
    const server = http.createServer((request, response) => {
        console.log(request.url)
        if(request.url.match('/twitch')) {
            this.twitchHandler.hookHandler(request, response)
        }
        if(request.url.match('/mixer')) {
            this.mixerHandler.hookHandler(request, response)         
        }
        response.end();
    });
    server.listen(3000);
};

webHookServer.createServer()
