const ServerUtility = require('../utility/server-utility')

class MixerHandler  {
  constructor() {
    this.serverUtility = new ServerUtility();
    this.mixerEventId = '';
  } 
}

MixerHandler.prototype.eventDupeCheck = function (requestBody) {
  console.log('dupecheck')
  if (this.mixerEventId == requestBody.id) {
    console.log(`id from request is ${requestBody.id}`)
    console.log(`id stored from most recent request is ${this.mixerEventId}`)
    console.log(`dupe event id. returning...`)
    return true
  } 
};

MixerHandler.prototype.sendHookData = function (requestBody) {
  console.log('hookdata ')
  if (!requestBody.payload.online) return; 
  console.log('attempting to send mixer data...')
  requestBody.eventOrigin = 'mixer'
  this.mixerEventId = requestBody.id
  this.serverUtility.socketConnect(requestBody)
};


MixerHandler.prototype.hookHandler = async function (request, response) {
  console.log('hooke handler')
  if(request.method !== 'POST') return;
  let requestBody = await this.serverUtility.getRequestBody(request)
  if(this.eventDupeCheck(requestBody)) return;
  this.sendHookData(requestBody)
  response.writeHead(200)
};

module.exports = MixerHandler