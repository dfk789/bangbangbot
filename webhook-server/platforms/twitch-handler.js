const ServerUtility = require('../utility/server-utility')

class TwitchHandler  {
  constructor() {
    this.serverUtility = new ServerUtility();
    this.challengeRegEx = /challenge=(.*)&hub.lease/;
    this.unSubRegEx = /challenge=(.*)&hub.mode=unsubscribe/;
  }
}

TwitchHandler.prototype.getChallengeToken = function (requestUrl) {
  let challengeToken
  //get sub token
  if (new RegExp(this.challengeRegEx).test(requestUrl) == true) {
    const match = requestUrl.match(this.challengeRegEx)
    challengeToken = match[1]
    return challengeToken
  }
  //get unsub token
  if (new RegExp(this.unSubRegEx).test(requestUrl) == true) {
    const match = requestUrl.match(this.unSubRegEx)
    challengeToken = match[1]
    return challengeToken
  }
};

TwitchHandler.prototype.challengeResponder = function (request, response) {
  if (!new RegExp(this.challengeRegEx).test(request.url)) return;

  const challengeToken = this.getChallengeToken(request.url)
  response.writeHead(200);
  response.write(challengeToken)
  console.log(`responding with challenge token at ${new Date()}`)
};

TwitchHandler.prototype.unSubChallengeResponder = function (request, response) {
  if (!new RegExp(this.unSubRegEx).test(request.url)) return;

  const challengeToken = this.getChallengeToken(request.url)
  console.log(`challenge token for unsub is: ${challengeToken}`)
  response.writeHead(200);
  response.write(challengeToken)
  console.log('unsubbing responding with challenge token')
};

TwitchHandler.prototype.sendHookData = async function (request) {
  if (request.method !== 'POST' || !request.url.match('twitch/streams')) return;

  let requestBody = await this.serverUtility.getRequestBody(request)
  requestBody.eventOrigin = 'twitch'
  this.serverUtility.socketConnect(requestBody)
};

TwitchHandler.prototype.hookHandler = function (request, response) {
  this.challengeResponder(request, response)
  this.unSubChallengeResponder(request, response)
  this.sendHookData(request)
}

module.exports = TwitchHandler