### config.json
* funGuy: The discord id of the jokester who you want to play with
* coolGuy:The discord id of the cool guy
* daveCopyPrefixes: Words that sometimes get added  to the start when davecopy is enabled
* daveCopySuffixes: Words that sometimes get added  to the end when davecopy is enabled
* amIRightReplies: Replies for when the cool guy says amiright
* funChannels: (Array) ids of which channels to enable fun features like change channelName
* streamAnnounceChannel: (string) id of channel in which to announce streams going live.

### private_config.json
* botClientId: (string) Your generated discord client id for the bot.
* twitchClientId: (string) Your twitch app client id
* callbackURL:(URL) the URL which twitch and mixer will send events to for stream announcments.
* mixerClientId: (string) Your mixer app client id
* mixerAuthorization: (string) Your mixer app auth secret ("Secret exampleifdsfsdfasd21321")

### stream_ids.json
* mixerIds:(array of strings):which mixer channels the bot will announce streams of.
* twitchIds:(array of strings):which twitch channels the bot will announce streams of.