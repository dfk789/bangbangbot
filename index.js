const Discord = require('discord.js')
const fs = require('fs');
const Bot = require('./bot/bot.js');
const config = require('./config/config.json')
let privateConfig
const funReplies = require('./bot/funreplies/config.json');
const streamIds = require('./config/stream_ids.json')

const thot = fs.readdirSync('./media/thots');
const client = new Discord.Client();

try{privateConfig = require('./config/my_private_config.json')}
catch{privateConfig = require('./config/private_config.json')}

if (!privateConfig.botClientId) throw new Error('Please configure the bot in ./config/private_config.json')

const addDirectory = (object) => {
  if (object.directory) {
    object.fsDir = fs.readdirSync(object.directory);
  }
  return object;
};

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled Rejection at:', reason.stack || reason)
})

new Bot({
  // merge private config into config
  config: Object.assign(config, privateConfig),
  funReplies: funReplies.map(addDirectory),
  client: client,
  streamIds: streamIds,
  fileFolders: {
    thot: thot,
  }
}).start();
