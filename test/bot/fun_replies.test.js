const Bot = require('../../bot/bot.js');
const config = require('../../config/config.json')

const mockClient = {};
let sendFunc
const createMockMessage = (message) => {
  sendFunc = jest.fn()
  return {
    content: message,
    channel: {
      send: sendFunc
    },
    author: {
      bot: false
    }
  };
};

const createBot = args => {
  return new Bot({
    client: mockClient,
    funReplies: [],
    fileFolders: {},
    config: config,
    streamIds: {twitchIds:[44776002]},
    ...args
  });
};


describe('bot replies only with a file', () => {

  test('blessed command & file', () => {
    const bot = createBot({
      funReplies: [{
        commands: 'blessed',
        file: './media/blessed.jpg',
      }]
    });
    bot.handleMessage(createMockMessage('blessed'));
    expect(sendFunc).toHaveBeenCalledWith({ file: './media/blessed.jpg' });
  });

  test('thot sends random thot file', () => {
    const bot = createBot({ fileFolders: { thot: ['thot1.jpg'] } });
    bot.handleMessage(createMockMessage('!thot'));
    expect(sendFunc).toHaveBeenCalledWith({ file: './media/thots/thot1.jpg' });
  });

  test('pog sends random pog', () => {
    const bot = createBot({
      funReplies: [{
        commands: '\\bpog\\b',
        directory: './media/pogs',
        fsDir: ['pog.jpg'],
        regex:true
      }]
    });
    bot.handleMessage(createMockMessage('pog'));
    expect(sendFunc).toHaveBeenCalledWith({ file: './media/pogs/pog.jpg' });
  });
})

describe('bot will find(match) all bangs', () => {

  test('match regular bang and only file', () => {
    const bot = createBot({
      funReplies: [{
        commands: 'bang',
        directory: './media/bangbang',
        fsDir: ['banging.jpg'],
        match:true
      }]
    });
    bot.handleMessage(createMockMessage('bang'));
    expect(sendFunc).toHaveBeenCalledWith({ file: './media/bangbang/banging.jpg' });
  });

  test('match hidden bang and only file', () => {
    const bot = createBot({
      funReplies: [{
        commands: 'bang',
        directory: './media/bangbang',
        fsDir: ['banging.jpg'],
        match: true,
      }]
    });
    bot.handleMessage(createMockMessage('hidebanghide'))
    expect(sendFunc).toHaveBeenCalledWith({ file: './media/bangbang/banging.jpg' })
  })
})



describe('bot replies only with a message', () => {

  test('bot replies one of two replies and only reply ', () => {
    const bot = createBot({
      funReplies: [{
        commands: "fuck anime",
        replies:["Lewd"]
      }]
    });
    bot.handleMessage(createMockMessage('fuck anime'))
    expect(sendFunc).toBeCalledWith("Lewd")
  })
})

