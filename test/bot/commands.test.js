/* eslint-disable no-undef */
const Bot = require('../../bot/bot.js');
const config = require('../../config/config.json')
const {getArgs, getCommand} = require('../../bot/commands/helpers')
const mockClient = {};
let sendFunc, replyFunc, editFunc

const createMockMessage = (message) => {
  sendFunc = jest.fn()
  return {
    content: message,
    channel: {
      send: sendFunc,
    },
    author: {
      bot: false
    }
  };
};

const createBot = args => {
  return new Bot({
    client: mockClient,
    funReplies: [],
    fileFolders: {},
    config: config,
    commands: {prefix:['!']},
    streamIds: {"twitchIds":["44776002"], "mixerIds":["5645453"]},
    ...args
  });
};

 

describe('test each bot command and bot responds accordingly.', () => {
   describe('twitch commands; bot calls function and sends message', ()=> {
    const bot = createBot(), startFunc = jest.fn(), stopFunc = jest.fn()
    bot.commands.twitchAnnounce = {
      getUserInfo:() => {return {id:'90210'}},
      startSubscriptions: () => {startFunc()},
      stopSubscription: () => {stopFunc()}
    }
    bot.commands.saveStreams = false

    test ('!addtwitch no username', async () => {
      await bot.handleMessage(createMockMessage('!addtwitch'))
      expect(startFunc).not.toBeCalled()
      expect(sendFunc).toHaveBeenCalledWith('please enter a username')
    });

    test('!addtwitch albert20202', async () => {
      await bot.handleMessage(createMockMessage('!addtwitch albert20202'))
      expect(startFunc).toBeCalled()
      expect(sendFunc).toHaveBeenCalledWith('I will now announce when albert20202(90210) goes live.');
    });

    test('!deltwitch albert20202', async () => {
      await bot.handleMessage(createMockMessage('!deltwitch albert20202'))
      expect(stopFunc).toBeCalled()
      expect(sendFunc).toHaveBeenCalledWith('I will no longer announce albert20202(90210) streams.')
    });
  })

  describe('mixer commands', () => {
    const bot = createBot(), createFunc = jest.fn(), deactivateFunc = jest.fn(), activeFunc = jest.fn()
    bot.commands.mixerHooks = {
      getChannelId:() => {return '90210'},
      createMixerHook: () => {createFunc()},
      deactivateHook: () => {deactivateFunc()},
      getActiveHookId: () =>{activeFunc()}
    }
    bot.commands.saveStreams = false
    test ('!addmixer no username', async () => {
      await bot.handleMessage(createMockMessage('!addmixer'))
      expect(createFunc).not.toBeCalled()
      expect(sendFunc).toHaveBeenCalledWith('please enter a username')
    });

    test('!addmixer albert20202', async () => {
      await bot.handleMessage(createMockMessage('!addmixer albert20202'))
      expect(createFunc).toBeCalled()
      expect(sendFunc).toHaveBeenCalledWith('I will now announce when albert20202(90210) goes live.');
    });

    test('!delmixer albert20202', async () => {
      await bot.handleMessage(createMockMessage('!delmixer albert20202'))
      expect(activeFunc).toBeCalled()
      expect(deactivateFunc).toBeCalled()
      expect(sendFunc).toHaveBeenCalledWith('I will no longer announce albert20202(90210) streams.')
    });
  })

  describe('get Command and Args correctly', () => {
    const bot = createBot()
    const mockCommand = {content:'!davename totallyNotDrunk haha you know'}
    
    test('get Args from msg', () => {
      const args = getArgs(mockCommand, bot.commands.prefix)
      expect(args[1]).toBe('totallyNotDrunk')
      expect(args[2]).toBe('haha')
      expect(args[3]).toBe('you')
      expect(args[4]).toBe('know')
    })

    test('get Command from msg', () => {
      const command = getCommand(mockCommand, bot.commands.prefix)
      expect(command).toBe('davename')
    })
  })

  describe('misc commands', () => {
    const bot = createBot(), replyFunc = jest.fn(), editFunc = jest.fn()
    
    test('bot responds on help command', () => {
      const mockMsg = {reply:replyFunc, content:'!help', author:'ChinaWarrior'}
      bot.handleMessage(mockMsg)
      expect(replyFunc).toBeCalled()
    })

    test('bot edits channel name', () => {
      const mockMsg = {
        reply:replyFunc, 
        content:'!channelname redman', 
        author:'ChinaWarrior', 
        channel:{id:'106520961381093376', 
        edit:editFunc
      }}
      bot.handleMessage(mockMsg)
      expect(editFunc).toHaveBeenCalledWith({name:' redman'})
    })
  })
})


