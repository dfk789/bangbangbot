const Bot = require('../../bot/bot.js');
const config = require('../../config/config.json')
const mockClient = {};
let sendFunc

const createMockMessage = (message) => {
  sendFunc = jest.fn()
  return {
    content: message,
    channel: {
      send: sendFunc
    },
    author: {
      bot: false
    }
  };
};

const createBot = args => {
  return new Bot({
    client: mockClient,
    funReplies: [],
    fileFolders: {},
    config: config,
    streamIds: {twitchIds:[44776002]},
    ...args
  });
};


test('when I say butcher bot says bacon', () => {
  const bot = createBot();
  bot.handleMessage(createMockMessage('when I say butcher the bot says bacon'));
  bot.handleMessage(createMockMessage('butcher'));
  expect(sendFunc).toHaveBeenCalledWith('bacon');
});

test('dynamic command handles url', () => {
  const bot = createBot();
  bot.handleMessage(createMockMessage('when I say dickpic the bot says http://something.jpg'));
  bot.handleMessage(createMockMessage('dickpic'));
  expect(sendFunc).toHaveBeenCalledWith('http://something.jpg');
});

test('multiple words in response', () => {
  const bot = createBot();
  bot.handleMessage(createMockMessage('When I say heave the bot says fuck that ho'));
  bot.handleMessage(createMockMessage('heave'));
  expect(sendFunc).toHaveBeenCalledWith('fuck that ho');
});

test('too many characters for dynamic command', () => {
  const bot = createBot();
  bot.handleMessage(createMockMessage('When I say diggeredoo the bot says diggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoodiggeredoo'));
  expect(sendFunc).toHaveReturned();
});

test('dynamic command handles spaces in response', () => {
  const bot = createBot();
  bot.handleMessage(createMockMessage('When I say poop the bot says "That smells"'));
  bot.handleMessage(createMockMessage('poop'));
  expect(sendFunc).toHaveBeenCalledWith('"That smells"');
});